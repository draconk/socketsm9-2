package com.server;

import sun.java2d.pipe.SpanIterator;

import java.rmi.AccessException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.DoubleSummaryStatistics;

/**
 * Created by ams2-04 on 11/03/2015.
 */
public class RMICalcServer implements RMICalcInterface{

    @Override
    public int suma(int a, int b) throws RemoteException {
        return (a + b);
    }

    @Override
    public int resta(int a, int b) throws RemoteException {
        return (a - b);
    }

    @Override
    public int multip(int a, int b) throws RemoteException {
        return  (a * b);
    }

    @Override
    public int div(int a, int b) throws RemoteException {
        return (a / b);
    }

    @Override
    public double potencia(int a, int b) throws RemoteException {
        return Math.pow(a, b);
    }

    public static void main(String[] args) {
        System.out.println("Creando el registro de objetos remotos...");

        Registry reg = null;
        try {
            reg = LocateRegistry.createRegistry(5555);
        } catch (RemoteException e) {
            System.out.println("ERROR: No se ha podido crear el registro");
            e.printStackTrace();
        }

        System.out.println("Creando el objeto servidor e inscribiendolo en el registro...");
        RMICalcServer serverObject = new RMICalcServer();

        try {
            reg.rebind("Calculadora", (RMICalcInterface) UnicastRemoteObject.exportObject(serverObject, 0));
        } catch (AccessException e) {
            System.out.println("ERROR: No se ha podido inscribir el objeto servidor.");
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
