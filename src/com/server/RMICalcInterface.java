package com.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by ams2-04 on 11/03/2015.
 */
public interface RMICalcInterface extends Remote{

    public int suma(int a,int b) throws RemoteException;
    public int resta(int a,int b) throws RemoteException;
    public int multip(int a,int b) throws RemoteException;
    public int div(int a,int b) throws RemoteException;
    public double potencia(int a, int b) throws RemoteException;


}
