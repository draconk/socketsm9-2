package com.server;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Created by ams2-04 on 11/03/2015.
 */
public class RMICalcClient {
    public static void main(String[] args) {

        RMICalcInterface calc = null;

        try {
            System.out.println("Localizando registro de objetos remotos...");
            Registry registry = LocateRegistry.getRegistry("localhost", 5555);
            System.out.println("Obteniendo el stub del objeto remoto...");
            calc = (RMICalcInterface) registry.lookup("Calculadora");
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        }

        if (calc != null){
            System.out.println("Realizando operaciones con el objeto remoto...");
            try{
                System.out.println("2 + 2 = " + calc.suma(2, 2));
                System.out.println("99 - 45 = " + calc.resta(99, 45));
                System.out.println("125 * 3 = " + calc.multip(125, 3));
                System.out.println("1250 / 5 = " + calc.div(1250, 5));
                System.out.println("8 ^ 8 = " + calc.potencia(8, 8));
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }
}
